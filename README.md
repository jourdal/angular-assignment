# AngularAssignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.1.

## Authors

- Daniel Jansson
- Jo Endre Brauten Urdal

### Install the pages:

npm install

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### The application is hosted with Heroku

https://angular-pokedex-noroff.herokuapp.com/catalogue
