import { Component, OnInit } from "@angular/core";
import { PokemonService } from "src/app/services/pokemons.service";
import { Pokemon } from "src/models/pokemon.model";

@Component({
  selector: "app-catalogue",
  templateUrl: "./catalogue.page.html",
  styleUrls: ["./catalogue.page.css"]
})
export class CataloguePage implements OnInit{

  get pokemons(): Pokemon[] {
    return this.pokemonService.pokemons;
  }

  constructor(private pokemonService: PokemonService) {}

  ngOnInit(): void {
    //Uses function from pokemonService to find all pokemons
    this.pokemonService.findAllPokemons();
  }

}