import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, map } from 'rxjs';
import { Pokemon, PokemonResponse } from '../../models/pokemon.model';

const URL_POKEMON = 'https://pokeapi.co/api/v2/pokemon?limit=898&offset=0';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  private _pokemons: Pokemon[] = [];
  private _loading: boolean = false;

  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private http: HttpClient) {}

  findAllPokemons(): void {
    this._loading = true;

    //Get request for all pokemons in the PokeApi
    this.http
      .get<PokemonResponse>(URL_POKEMON)
      .pipe(
        map((response: PokemonResponse) => {
          return response.results;
        }),
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (pokemons: Pokemon[]) => {
          this._pokemons = pokemons;
          //For loop to att the pokemon name inside of session storage.
          for (let i = 0; i < this._pokemons.length; i++) {
            const pokeNames = this._pokemons[i].name;
            sessionStorage.setItem(
              pokeNames,
              JSON.stringify(this._pokemons[i])
            );
          }
        },
      });
  }
}
