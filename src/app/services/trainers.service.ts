import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from 'src/models/trainer.model';

const trainerURL = environment.trainerAPI;
const trainerAPI_KEY = environment.trainerAPI_KEY;
export const USER_KEY = 'username';

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  private _username: string = '';


  get username(): string {
    return this._username;
  }

  set username(username: string) {
    sessionStorage.setItem(USER_KEY, username);
    this._username = username;
  }

  // DI - Dependency Injection
  constructor(private http: HttpClient) {
    this._username = sessionStorage.getItem(USER_KEY) || '';
  }

  // Header for POST request
  private createHeaders() {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'X-API-KEY': trainerAPI_KEY,
    });
  }

 

  // POST Request
  registerTrainer(username: string): Observable<Trainer> {
    const headers = this.createHeaders();
    const trainer = { username, pokemon: [] };
    return this.http.post<Trainer>(trainerURL, trainer, { headers });
  }

  // GET request by username
  public findTrainerByUsername(
    username: string
  ): Observable<Trainer | undefined> {
    return this.http
      .get<Trainer[]>(`${trainerURL}/?username=${username}`)
      .pipe(map((trainers) => trainers.pop()));
  } 
}
