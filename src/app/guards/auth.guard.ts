import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { TrainerService, USER_KEY } from '../services/trainers.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private trainerService: TrainerService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
      //If there is a username in session storage, the guard will let you continue on the page.
    if (sessionStorage.getItem(USER_KEY)) {
      return true;
    } else {
      this.router.navigateByUrl('/'); // Back to login
      return false;
    }
  }
}
