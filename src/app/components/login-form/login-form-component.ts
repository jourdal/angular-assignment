import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { TrainerService, USER_KEY } from 'src/app/services/trainers.service';
import { Trainer } from 'src/models/trainer.model';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
})
export class LoginFormComponent implements OnInit {
  constructor(private trainerService: TrainerService, private router: Router) {}

  // Redirects to catalogue if a user already exists in session storage
  ngOnInit(): void {
    if (sessionStorage.getItem(USER_KEY)) {
      this.router.navigateByUrl('/catalogue');
    }
  }

  public onLoginSubmit(loginForm: NgForm): void {
    const { username } = loginForm.value;
    //Get request for user by user ID
    this.trainerService.findTrainerByUsername(username).subscribe({
      next: (trainer: Trainer | undefined) => {
        // Checks for user in API
        if (trainer !== undefined) {
          const existingTrainer = trainer;
        } else {
          // POST request
          this.trainerService.registerTrainer(username).subscribe({
            next: (response: any) => {},
          });
        }
        // Saves user in storage and redirects
        sessionStorage.setItem(USER_KEY, username);
        this.router.navigateByUrl('/catalogue');
        setTimeout(function () {
          window.location.reload();
        }, 0);
      },
    });
  }
}
