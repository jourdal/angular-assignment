import { Component, Input, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TrainerService } from 'src/app/services/trainers.service';
import { Trainer } from 'src/models/trainer.model';
import { environment } from 'src/environments/environment';

const trainerURL = environment.trainerAPI;
const trainerAPI_KEY = environment.trainerAPI_KEY;

@Component({
  selector: 'app-trainer-profile',
  templateUrl: './trainer-profile.component.html',
  styleUrls: ['./trainer-profile.component.css'],
})
export class TrainerProfileComponent implements OnInit {
  pokemonArray: [] = [];
  pokeIndexes: string[] = [];
  pokeUrl: string =
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';
  pokePng: string = '.png';

  constructor(
    private trainerService: TrainerService,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    //This segment of code will get the ID of the pokemons caught, so the correct picture can be displayed.
    const username = this.trainerService.username;
    this.trainerService.findTrainerByUsername(username).subscribe({
      next: (trainer: Trainer | undefined) => {
        if (trainer !== undefined) {
          const existingTrainer = trainer;
          this.pokemonArray = existingTrainer.pokemon;
          //This loop will go through the length of the pokemon array of the users caught pokemon.
          for (let i = 0; i < this.pokemonArray.length; i++) {
            let pokemonInSess = sessionStorage.getItem(this.pokemonArray[i]);
            if (pokemonInSess !== null) {
              const pokemonInSessData = JSON.parse(pokemonInSess);
              //removes the part before the ID and the last "/" of the URL.
              const pokeIndex = pokemonInSessData.url.slice(34, -1);
              this.pokeIndexes.push(pokeIndex);
            }
          }
        }
      },
    });
  }
  //creating httpHeader for easy use later in the app.
  private createHeaders() {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'X-API-KEY': trainerAPI_KEY,
    });
  }
  //The pokemon that is clicked will be released from the trainers API list,
  //the webpage will do a quick reload and the removed pokemon will be gone from the site.
  releaseMe(pokemon: never) {
    const headers = this.createHeaders();
    const username = this.trainerService.username;

    //Get request.
    this.trainerService.findTrainerByUsername(username).subscribe({
      next: (trainer: Trainer | undefined) => {
        //Use the getter to fetch the array of pokemon, and patch new list of pokemon.
        if (trainer !== undefined) {
          const existingTrainer = trainer;
          const selectedPokemon = pokemon;
          let fetchedPokeArray = existingTrainer.pokemon;
          //This segment will remove the pokemon clicked from the array fetch from the api.
          const index = fetchedPokeArray.indexOf(selectedPokemon);
          if (index > -1) {
            fetchedPokeArray.splice(index, 1);
          }

          const newPokeList = {
            pokemon: fetchedPokeArray,
          };

          this.http
            .patch(`${trainerURL}/${existingTrainer.id}`, newPokeList, {
              headers,
            })
            .subscribe();

          //Added to reload the page.
          setTimeout(function () {
            window.location.reload();
          }, 250);
        }
      },
    });
  }
}
