import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/models/pokemon.model';
import { TrainerService } from 'src/app/services/trainers.service';
import { environment } from 'src/environments/environment';
import { Trainer } from 'src/models/trainer.model';
import Swal from 'sweetalert2';

const trainerURL = environment.trainerAPI;
const trainerAPI_KEY = environment.trainerAPI_KEY;

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css'],
})
export class PokemonListComponent implements OnInit {
  //Variables used for the pokemon-list-component
  pokeUrl: string =
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';
  pokePng: string = '.png';
  pokeListCheck: string[] = [];
  pokeBallPic: string =
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/poke-ball.png';
  @Input() pokemons: Pokemon[] = [];

  constructor(
    private trainerService: TrainerService,
    private http: HttpClient
  ) {}

  ngOnInit(): void {}
  //creating httpHeader for easy use later in the app.
  private createHeaders() {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'X-API-KEY': trainerAPI_KEY,
    });
  }

  //When the user clicks on the button Catch Me!, this code will run,
  //get the previous pokemons and patch inn the old pokemons.
  catchMe(pokemon: Pokemon) {
    const headers = this.createHeaders();
    const username = this.trainerService.username;
    //get request to api
    this.trainerService.findTrainerByUsername(username).subscribe({
      next: (trainer: Trainer | undefined) => {
        if (trainer !== undefined) {
          const existingTrainer = trainer;
          let newPokemon = '';
          //checks if there is any pokemon that has been caught by the trainer already, to avoid empty array element.
          if (existingTrainer.pokemon.toString() == '') {
            newPokemon = pokemon.name;
          } else {
            newPokemon = existingTrainer.pokemon + ',' + pokemon.name;
          }
          const pokeList = newPokemon.split(',');
          this.pokeListCheck = pokeList;
          //Object to be inserted to the api with the fetch request.
          const newUsername = {
            pokemon: pokeList,
          };
          //patch request to api
          this.http
            .patch(`${trainerURL}/${existingTrainer.id}`, newUsername, {
              headers,
            })
            .subscribe();
          Swal.fire('Congratulations, you caught: ' + pokemon.name);
          //Added to reload the page.
        }
      },
    });
  }
}
