//Interface with the values of a trainer
export interface Trainer {
    id: number;
    username: string;
    pokemon: [];
}