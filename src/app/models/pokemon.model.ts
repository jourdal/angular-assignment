//Interface with the values fetched by the PokeApi
export interface PokemonResponse{
  count: number;
  next: string;
  previous: boolean;
  results: Pokemon[];
}
//Interface with the values fetched by the PokeApi
export interface Pokemon{
  name: string;
  url: string;
}